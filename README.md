# Gilab Code Quality Report Tool

This project has a set of VIs for parsing the LabVIEW VI Analyzer Report into Gitlab Code Quality JSON Format.

The use is intented for CI when running for example VI Analyzer in a job and you want to visualize the results in the Gitlab Code Quality Widget.

See example:

![](docs/code-quality-gitlab.png)

More about this Gitlab Feature - https://docs.gitlab.com/ee/ci/testing/code_quality.html

## Author and Contributor

Felipe Pinheiro Silva

Kudos to Darren Nattinger for providing the VI Analyzer report parser from RSL to cluster.

https://forums.ni.com/t5/VI-Analyzer-Enthusiasts/VI-Analyzer-RSL-Exporter/ta-p/3494545

## Compatibility

LabVIEW 2020 or later

## Usage - Command line

Usage is through command line and G-CLI.

Be aware that the tool generates clickable relative paths in the widget. In order to make it functional it uses the "Working Directory (where g-cli was launched)" as base path.

Therefore it is REQUIRED that the generated VI Analyzer report have the VI paths with a common path to the "Working Directory".
```
usage: g-cli  gitlab-code-quality-report -- [-v PATH] [-r PATH]

Generate Gitlab Code Quality Report from VI Analyzer RSL Report

 arguments:
  -v PATH, --vian-rsl PATH               Path for VI Analyzer RSL report
  -r PATH, --report-path PATH            Destination path for the JSON Code Quality Report
```

See example of a use:

![](docs/command-line.png)

### VI Usage

If the command line does not fit your workflow, you can directly use the VIs from the library in your project.

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
